﻿using AutoMapper;
using CarStore.Domain.Models;

namespace CarStore.Domain.Infrastructire
{
    public class DomainMapperProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Car, Car>()
                .ForMember(car => car.Id, opt => opt.Ignore());

            Mapper.CreateMap<Car, CarStore.DataAccess.Models.Car>();
            Mapper.CreateMap<CarStore.DataAccess.Models.Car, Car>();

            Mapper.CreateMap<User, CarStore.DataAccess.Models.User>();
            Mapper.CreateMap<CarStore.DataAccess.Models.User, User>();
        }
    }
}