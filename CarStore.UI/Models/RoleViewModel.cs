﻿using Microsoft.AspNet.Identity;

namespace CarStore.UI.Models
{
    public class RoleViewModel : IRole<int>
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}