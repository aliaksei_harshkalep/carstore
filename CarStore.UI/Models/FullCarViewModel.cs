﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CarStore.UI.Models
{
    public class FullCarViewModel
    {
        public Guid Id { get; set; }
        public bool InStock { get; set; }
        [Required]
        [MinLength(3)]
        public string Name { get; set; }
        [Range(0.5, 6)]
        public float Engine { get; set; }
        [Range(40, 350)]
        public int MaxSpeed { get; set; }
        public DateTime Date { get; set; }
        [MaxLength(1024)]
        public string Description { get; set; }
    }
}