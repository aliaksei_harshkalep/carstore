﻿using AutoMapper;
using CarStore.Domain.Models;
using CarStore.UI.Models;

namespace CarStore.UI
{
    public class UIMapperProfile : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Car, CarViewModel>();

            Mapper.CreateMap<Car, FullCarViewModel>();
            Mapper.CreateMap<FullCarViewModel, Car>();

            Mapper.CreateMap<UserViewModel, User>();
            Mapper.CreateMap<User, UserViewModel>();

            base.Configure();
        }
    }
}