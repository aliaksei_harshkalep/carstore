﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using CarStore.DataAccess.JsonParsing;
using CarStore.Domain.Models;

namespace CarStore.Domain.Repositories
{
    public class JsonUserRepository : IUserRepository
    {
        private readonly JsonDataContext _context;

        public JsonUserRepository(JsonDataContext context)
        {
            _context = context;
        }

        public IEnumerable<User> GetUsers()
        {
            if (!_context.IsInitialized) _context.Initialize();

            return Mapper.Map<IEnumerable<User>>(_context.Users);
        }

        public User GetById(Guid id)
        {
            if (!_context.IsInitialized) _context.Initialize();


            var dataAccessUser = _context.Users.FirstOrDefault(u => u.Id == id);
            return Mapper.Map<User>(dataAccessUser);
        }
    }
}