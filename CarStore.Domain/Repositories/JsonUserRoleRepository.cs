﻿using System;
using System.Collections.Generic;
using System.Linq;
using CarStore.DataAccess.JsonParsing;
using CarStore.Domain.Models;

namespace CarStore.Domain.Repositories
{
    public class JsonUserRoleRepository : IUserRolesRepository
    {
        private readonly JsonDataContext _dataContext;

        public JsonUserRoleRepository(JsonDataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public IEnumerable<Role> GetUserRoles(Guid userId)
        {
            if (!_dataContext.IsInitialized) _dataContext.Initialize();

            return _dataContext.UserRoles.Where(ur => ur.UserId == userId).Select(ur => (Role) ur.RoleId);
        }
    }
}