﻿using CarStore.DataAccess.JsonParsing;
using Ninject;
using Ninject.Modules;
using Ninject.Parameters;

namespace CarStore.DataAccess
{
    public class DataAccessModule : NinjectModule
    {
        public override void Load()
        {
            Kernel.Bind<JsonDataContext>()
                .ToSelf()
                .WithConstructorArgument(new ConstructorArgument("fileName", ctx => ctx.Kernel.Get<string>("DataFile")));
        }
    }
}