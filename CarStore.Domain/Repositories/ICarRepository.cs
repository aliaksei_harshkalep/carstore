﻿using System.Collections.Generic;
using CarStore.Domain.Models;

namespace CarStore.Domain.Repositories
{
    public interface ICarRepository
    {
        IEnumerable<Car> GetCars();
        void UpdateCar(Car car);
    }
}