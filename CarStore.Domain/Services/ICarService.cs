﻿using System;
using System.Collections.Generic;
using CarStore.Domain.Models;

namespace CarStore.Domain.Services
{
    public interface ICarService
    {
        IEnumerable<Car> ShowAvaliableCars();
        IEnumerable<Car> EditCars();
        void EditCar(Car car);
        Car ShowCarDetails(Guid carId);
    }
}