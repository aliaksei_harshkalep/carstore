﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AutoMapper;
using CarStore.Domain.Models;
using CarStore.Domain.Repositories;
using CarStore.Domain.Services;
using CarStore.UI.Authentication;
using CarStore.UI.Models;
using Microsoft.AspNet.Identity;

namespace CarStore.UI.Controllers
{
    [Authorize(Roles = "Admin")]
    public class AdminController : Controller
    {
        private readonly ICarService _carService;

        public AdminController(ICarService carService)
        {
            _carService = carService;

            
        }

        public ActionResult Index()
        {
            var carsList = _carService.EditCars();

            var viewModel = Mapper.Map<IEnumerable<CarViewModel>>(carsList);

            return View(viewModel);
        }

        public ActionResult Edit(Guid id)
        {
            var car = _carService.ShowCarDetails(id);

            var viewModel = Mapper.Map<FullCarViewModel>(car);

            return View(viewModel);
        }

        [HttpPost]
        public ActionResult Edit(FullCarViewModel car)
        {
            if (!ModelState.IsValid)
            {
                return View(car);
            }

            var domainCar = Mapper.Map<Car>(car);

            _carService.EditCar(domainCar);

            return RedirectToAction("Index");
        }
    }
}