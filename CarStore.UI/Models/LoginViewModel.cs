﻿namespace CarStore.UI.Models
{
    public class LoginViewModel
    {
        public string UserName { get; set; }
        public string Password { get; set; } 
    }
}