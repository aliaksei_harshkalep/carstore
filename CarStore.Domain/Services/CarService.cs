﻿using System;
using System.Collections.Generic;
using System.Linq;
using CarStore.Domain.Models;
using CarStore.Domain.Repositories;

namespace CarStore.Domain.Services
{
    public class CarService : ICarService
    {
        private readonly ICarRepository _carRepository;

        public CarService(ICarRepository carRepository)
        {
            _carRepository = carRepository;
        }

        public IEnumerable<Car> ShowAvaliableCars()
        {
            var avaliableCars = _carRepository.GetCars().Where(car => car.InStock).ToList();

            return avaliableCars;
        }

        public IEnumerable<Car> EditCars()
        {
            var allCars = _carRepository.GetCars().ToList();

            return allCars;
        }

        public void EditCar(Car car)
        {
            if (car == null) throw new ArgumentNullException(nameof(car));

            _carRepository.UpdateCar(car);
        }

        public Car ShowCarDetails(Guid carId)
        {
            var showingCar = _carRepository.GetCars().FirstOrDefault(car => car.Id == carId);
            
            if(showingCar == null) throw new ArgumentException("There is no cars with the specified id", nameof(carId));

            return showingCar;
        }
    }
}