﻿using System;
using System.Collections.Generic;
using CarStore.Domain.Models;

namespace CarStore.Domain.Repositories
{
    public interface IUserRepository
    {
        IEnumerable<User> GetUsers();
        User GetById(Guid id);
    }
}