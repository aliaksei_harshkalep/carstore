﻿using CarStore.Domain.Repositories;
using CarStore.Domain.Services;
using Ninject.Modules;

namespace CarStore.Domain
{
    public class DomainModule : NinjectModule
    {
        public override void Load()
        {
            Kernel.Bind<ICarService>().To<CarService>();
            Kernel.Bind<ICarRepository>().To<JsonCarRepository>();
            Kernel.Bind<IUserRepository>().To<JsonUserRepository>();
            Kernel.Bind<IUserRolesRepository>().To<JsonUserRoleRepository>();
        }
    }
}