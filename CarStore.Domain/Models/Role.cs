﻿namespace CarStore.Domain.Models
{
    public enum Role
    {
        User,
        Admin,
        Manager
    }
}