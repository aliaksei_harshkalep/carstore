﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using AutoMapper;
using CarStore.Domain.Repositories;
using CarStore.Domain.Services;
using CarStore.UI.Models;

namespace CarStore.UI.Controllers
{
    public class CarsController : Controller
    {
        private readonly ICarService _carService;

        public CarsController(ICarService carService)
        {
            _carService = carService;
        }

        public ActionResult Index()
        {
            var carsList = _carService.ShowAvaliableCars();

            var viewModel = Mapper.Map<IEnumerable<CarViewModel>>(carsList);

            return View(viewModel);
        }

        public ActionResult Details(Guid id)
        {
            var car = _carService.ShowCarDetails(id);

            var viewModel = Mapper.Map<FullCarViewModel>(car);

            return View(viewModel);
        }
    }
}