﻿using System;

namespace CarStore.DataAccess.Models
{
    public class UserRole
    {
        public Guid UserId { get; set; }
        public int RoleId { get; set; } 
    }
}