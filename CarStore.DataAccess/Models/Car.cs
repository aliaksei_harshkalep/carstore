﻿using System;

namespace CarStore.DataAccess.Models
{
    public class Car
    {
        public Guid Id { get; set; }
        public bool InStock { get; set; }
        public string Name { get; set; }
        public float Engine { get; set; }
        public int MaxSpeed { get; set; }
        public string Description { get; set; }
        public DateTime Date { get; set; }
    }
}