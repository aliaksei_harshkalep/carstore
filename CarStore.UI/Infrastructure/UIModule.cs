﻿using System.Web;
using CarStore.UI.Authentication;
using CarStore.UI.Models;
using Microsoft.AspNet.Identity;
using Ninject.Modules;

namespace CarStore.UI.Infrastructure
{
    public class UIModule : NinjectModule
    {
        public override void Load()
        {
            Kernel.Bind<UserManager<UserViewModel, string>>().ToSelf();
            Kernel.Bind<IUserStore<UserViewModel, string>>().To<CustomUserStore>();
            Kernel.Bind<string>().ToMethod(ctx => HttpContext.Current != null 
                ? HttpContext.Current.Server.MapPath("~/App_Data/Data.json")
                : null);
        }
    }
}