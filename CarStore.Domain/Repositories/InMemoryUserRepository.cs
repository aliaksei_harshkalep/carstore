﻿using System;
using System.Collections.Generic;
using System.Linq;
using CarStore.Domain.Models;

namespace CarStore.Domain.Repositories
{
    public class InMemoryUserRepository : IUserRepository
    {
        private static readonly List<User> Users = new List<User>
        {
            new User
            {
                Id = new Guid("F57BF3A9-E77F-45BA-9768-0EFAB07CE208"),
                Password = "secret",
                UserName = "Admin"
            },
            new User
            {
                Id = new Guid("3CA11258-2B5E-4CEC-86E8-08D0BAC8A383"),
                Password = "secret",
                UserName = "User"
            }
        };

        public IEnumerable<User> GetUsers()
        {
            return Users;
        }

        public User GetById(Guid id)
        {
            return Users.FirstOrDefault(u => u.Id == id);
        }
    }
}