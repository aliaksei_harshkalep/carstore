﻿using System;
using System.Collections.Generic;
using System.IO;
using CarStore.DataAccess.Models;
using Newtonsoft.Json;

namespace CarStore.DataAccess.JsonParsing
{
    public class JsonDataContext
    {
        private readonly string _fileName;
        public List<Car> Cars { get; set; }
        public List<User> Users { get; set; }
        public List<UserRole> UserRoles { get; set; }
        public bool IsInitialized { get; private set; }

        public JsonDataContext(string fileName)
        {
            _fileName = fileName;
            Cars = new List<Car>();
            Users = new List<User>();
            UserRoles = new List<UserRole>();
        }

        public void Initialize()
        {
            using (var sr = new StreamReader(_fileName))
            {
                var value = sr.ReadToEnd();
                var data = JsonConvert.DeserializeObject<JsonDataContext>(value);

                Cars = data.Cars;
                Users = data.Users;
                UserRoles = data.UserRoles;

                IsInitialized = true;
            }
        }

        public void InitializeWithFakeData()
        {
            Cars.AddRange(new Car[]
            {
                new Car
                {
                    Id = Guid.NewGuid(),
                    Name = "Aston Martin",
                    InStock = false,
                    Description = "James Bond car",
                    MaxSpeed = 280,
                    Engine = 4.5f,
                    Date = DateTime.Today
                },
                new Car
                {
                    Id = Guid.NewGuid(),
                    Name = "Ford",
                    InStock = true,
                    Description = "Just a comfortable car",
                    MaxSpeed = 200,
                    Engine = 2f,
                    Date = DateTime.Today
                },
                new Car
                {
                    Id = Guid.NewGuid(),
                    Name = "Porsche 911",
                    InStock = true,
                    Description = "Nice sport car",
                    MaxSpeed = 250,
                    Engine = 3f,
                    Date = DateTime.Today
                },
                new Car
                {
                    Id = Guid.NewGuid(),
                    Name = "BMW",
                    InStock = true,
                    Description = "Reliable German car",
                    MaxSpeed = 220,
                    Engine = 4.0f,
                    Date = DateTime.Today
                },
                new Car
                {
                    Id = Guid.NewGuid(),
                    Name = "Shkoda",
                    InStock = true,
                    Description = "Cheep car for everybody",
                    MaxSpeed = 185,
                    Engine = 1.5f,
                    Date = DateTime.Today
                }
            });

            Users.AddRange(new User[]
            {
                new User
                {
                    Id = new Guid("F57BF3A9-E77F-45BA-9768-0EFAB07CE208"),
                    Password = "secret",
                    UserName = "Admin"
                },
                new User
                {
                    Id = new Guid("3CA11258-2B5E-4CEC-86E8-08D0BAC8A383"),
                    Password = "secret",
                    UserName = "User"
                }
            });

            UserRoles.AddRange(new UserRole[]
            {
                new UserRole
                {
                    UserId = new Guid("F57BF3A9-E77F-45BA-9768-0EFAB07CE208"),
                    RoleId = 1
                },
                new UserRole
                {
                    UserId = new Guid("F57BF3A9-E77F-45BA-9768-0EFAB07CE208"),
                    RoleId = 0
                },
                new UserRole
                {
                    UserId = new Guid("3CA11258-2B5E-4CEC-86E8-08D0BAC8A383"),
                    RoleId = 0
                },
            });

            SaveChanges();
        }

        public void SaveChanges()
        {
            using (var sw = new StreamWriter(_fileName, false))
            {
                var value = JsonConvert.SerializeObject(this);
                sw.Write(value);
            }
        }
    }
}