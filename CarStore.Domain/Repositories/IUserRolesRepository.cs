﻿using System;
using System.Collections;
using System.Collections.Generic;
using CarStore.Domain.Models;

namespace CarStore.Domain.Repositories
{
    public interface IUserRolesRepository
    {
        IEnumerable<Role> GetUserRoles(Guid userId);
    }
}