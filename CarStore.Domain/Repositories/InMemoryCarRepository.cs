﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using CarStore.Domain.Models;

namespace CarStore.Domain.Repositories
{
    public class InMemoryCarRepository : ICarRepository
    {
        private static readonly List<Car> Cars = new List<Car>
        {
            new Car
            {
                Id = Guid.NewGuid(),
                Name = "Aston Martin",
                InStock = false,
                Description = "James Bond car",
                MaxSpeed = 280,
                Engine = 4.5f,
                Date = DateTime.Today
            },
            new Car
            {
                Id = Guid.NewGuid(),
                Name = "Ford",
                InStock = true,
                Description = "Just a comfortable car",
                MaxSpeed = 200,
                Engine = 2f,
                Date = DateTime.Today
            },
            new Car
            {
                Id = Guid.NewGuid(),
                Name = "Porsche 911",
                InStock = true,
                Description = "Nice sport car",
                MaxSpeed = 250,
                Engine = 3f,
                Date = DateTime.Today
            },
            new Car
            {
                Id = Guid.NewGuid(),
                Name = "BMW",
                InStock = true,
                Description = "Reliable German car",
                MaxSpeed = 220,
                Engine = 4.0f,
                Date = DateTime.Today
            },
            new Car
            {
                Id = Guid.NewGuid(),
                Name = "Shkoda",
                InStock = true,
                Description = "Cheep car for everybody",
                MaxSpeed = 185,
                Engine = 1.5f,
                Date = DateTime.Today
            },
        };

        public IEnumerable<Car> GetCars()
        {
            return Cars;
        }

        public void UpdateCar(Car car)
        {
            var existingCar = Cars.FirstOrDefault(c => c.Id == car.Id);

            if (existingCar == null) throw new InvalidOperationException();

            Mapper.Map(car, existingCar);
        }
    }
}