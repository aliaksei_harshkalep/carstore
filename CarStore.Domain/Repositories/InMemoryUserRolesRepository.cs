﻿using System;
using System.Collections.Generic;
using System.Linq;
using CarStore.Domain.Models;

namespace CarStore.Domain.Repositories
{
    public class InMemoryUserRolesRepository : IUserRolesRepository
    {
        private static readonly List<Tuple<Guid, Role>> UserRoles = new List<Tuple<Guid, Role>>
        {
            new Tuple<Guid, Role>(new Guid("F57BF3A9-E77F-45BA-9768-0EFAB07CE208"), Role.Admin),
            new Tuple<Guid, Role>(new Guid("F57BF3A9-E77F-45BA-9768-0EFAB07CE208"), Role.Manager),
            new Tuple<Guid, Role>(new Guid("3CA11258-2B5E-4CEC-86E8-08D0BAC8A383"), Role.User),
        }; 

        public IEnumerable<Role> GetUserRoles(Guid userId)
        {
            return UserRoles.Where(ur => ur.Item1 == userId).Select(ur => ur.Item2);
        }
    }
}