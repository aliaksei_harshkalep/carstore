﻿using System.Collections.Generic;
using AutoMapper;
using CarStore.DataAccess.JsonParsing;
using CarStore.Domain.Models;

namespace CarStore.Domain.Repositories
{
    public class JsonCarRepository : ICarRepository
    {
        private readonly JsonDataContext _dataContext;

        public JsonCarRepository(JsonDataContext dataContext)
        {
            _dataContext = dataContext;
        }

        public IEnumerable<Car> GetCars()
        {
            if (!_dataContext.IsInitialized) _dataContext.Initialize();

            return Mapper.Map<IEnumerable<Car>>(_dataContext.Cars);
        }

        public void UpdateCar(Car car)
        {
            if (!_dataContext.IsInitialized) _dataContext.Initialize();

            _dataContext.Cars.RemoveAll(c => c.Id == car.Id);
            var dataAccessModel = Mapper.Map<CarStore.DataAccess.Models.Car>(car);

            _dataContext.Cars.Add(dataAccessModel);
            _dataContext.SaveChanges();
        }
    }
}