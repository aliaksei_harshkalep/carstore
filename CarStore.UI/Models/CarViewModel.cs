﻿using System;

namespace CarStore.UI.Models
{
    public class CarViewModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public float Engine { get; set; }
        public int MaxSpeed { get; set; }
    }
}